<!-- The Modal -->
<div class="modal" id="myModal" v-if="modalUser">
  <div class="modal-dialog modal-dialog-custom">
    <div class="modal-content modal-content-cumston">
    
      <div class="modal-header">
        <h4 class="modal-title">Editar Usuario</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <div class="modal-body">
        <blockquote class="blockquote mb-0">
          <form @submit.prevent="editUserForm">

            <fieldset>
              
              <div class="form-group row row-form" hidden>
                <div class="col-md-8">
                  <input id="id" v-model="dataEditUser.id" type="text" placeholder="id" class="form-control">
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-6" >
                  <input v-model="dataEditUser.nom_usuario" type="text" placeholder="Nombres" name="nombres"
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('nombres')">{{errors.first('nombres')}}</span>
                </div>

                <div class="col-md-6">
                  <input name="Apellidos" v-model="dataEditUser.ape_usuario" type="text" placeholder="Apellidos" 
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('Apellidos')">{{errors.first('Apellidos')}}</span>
                </div>
              </div>
              
              <div class="form-group row row-form">
                <div class="col-md-6">
                  <select class="browser-default custom-select form-control" name="Perfil" v-model="dataEditUser.id_perfil" v-validate="'required'">
                    <option selected v-for="(option, index) in dataPerfil" v-bind:value="option.id">{{ option.nom_perfil }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Perfil')">{{errors.first('Perfil')}}</span>

                </div>

                <div class="col-md-6">

                  <select class="browser-default custom-select form-control" v-model="dataEditUser.id_areas" name="Area" v-validate="'required'">
                    <option selected v-for="(option, index) in dataArea" v-bind:value="option.id">{{ option.nom_area }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Area')">{{errors.first('Area')}}</span>
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-12">
                  <select class="browser-default custom-select form-control" name="Activo"v-model="dataEditUser.activ" v-validate="'required'">
                    <option selected v-for="(option, index) in dataActivo" v-bind:value="option.id">{{ option.activo }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Activo')">{{errors.first('Activo')}}</span>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btn-primary btn-lg btn-form btn-add-user">Guardar</button>
                </div>
              </div>
            </fieldset>
            
            
          </form>
        </blockquote>
      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->
      
    </div>
  </div>
</div>