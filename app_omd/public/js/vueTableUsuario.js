//valida los formularios si los campos son requeridos
Vue.use(VeeValidate);
//instancia de vuejs con el id de la pagina  de usuarios
var app3 = new Vue({
  el: '#app-table',
  data : {
  		message : '',
	  	error : false,
	  	tableBody : false,
	  	tableData : [],
	  	modalUser : false,
	  	dataPerfil : [],
	  	dataArea : [],
	  	dataActivo : [],

	  	dataEditUser : {
	  		id : null,
	  		activo : true,
	  		nom_area : null,
	  		nom_perfil : null,
	  		nom_usuario : null,
	  		ape_usuario : null, 
	  	},
  },

  mounted : function (){
  	this.getData();
  },

  methods : {

  	getData: function () {
  		fetch('../../../app_omd/controllers/router.php?accion=getUsers', {
			  method: 'GET',
			  headers:{
			    'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => {
				let status = response.status;
				if(status == 200){
					this.tableBody = true;
					this.tableData = response.data;
				}else if(status == 201){

				}else if(status == 500){
					
				}
			});
  	},

  	editUser: function (dataUser) {
  		this.dataEditUser = Object.assign({}, dataUser);
  		
  		console.log(this.dataEditUser)
  		this.getAllData();
  		this.modalUser = true;
  	},

  	editUserForm: function(){
  		this.$validator.validateAll().then(() => {
  			console.log(this.dataEditUser)
      }).catch(() => {
        console.log(this.errors.all())
        return false
      })
  		//if (!this.errors.any()) {}
  	},

  	getAllData: function() {
  		fetch('../../../app_omd/controllers/router.php?accion=getAllData', {
			  method: 'GET',
			  headers:{
			    'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => {
				let status = response.status;
				if(status == 200){
					this.dataActivo = [
						{ activo : 'Activo', id : 1 },
						{ activo : 'Inactivo', id : 0 }
					];
					this.dataPerfil = response.perfil;
					this.dataArea = response.areas;
				}else if(status == 201){

				}else if(status == 500){
					
				}
			});

  	}
  },
   
  created () {
  	
  }
})