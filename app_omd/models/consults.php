<?php 
require_once 'connexion.php';

/**
 * 
 */
class Consults
{
	private $sql;
	private $query;
	private $sql2;
	private $query2;
	private $sqlValidate;
	private $queryValidate;
	private $res;
	
	function __construct()
	{
		$this->connect = new connexion();
		$this->connect->getConnection();
		# code...
	}

	public function authUsers(Request $request)
	{
		try {
			$this->res= array();
			$user = $request->request['userName'];
			$pass = $request->request['userPass'];
			$this->sql ="SELECT * 
									 FROM usuario 
									 WHERE usuario = '$user' 
									 AND contrasena = '$pass'";
			$this->query = $this->connect->connect->prepare($this->sql);
			$this->query->execute();
			if($this->query->rowCount() > 0 ){
				$data = $this->query->fetchAll();
				$this->res['data'] = $data;
				$this->res['status'] = 200;
			}else{
				$this->res['status'] = 201;
			}
		} catch (PDOException  $e) {
			$this->res['status'] = 500;
			//echo $e->getMessage();
		} finally {
			return $this->res;
		}
		
	}

	public function getUsers()
	{
		try {
			$this->res= array();
			$this->sql="SELECT 
										u.id AS id, 
										u.nom_usuario AS nom_usuario, 
										u.ape_usuario AS ape_usuario, 
										u.usuario AS usuario, 
										CASE WHEN  activo = 1 THEN 'Activo' ELSE 'Inactivo' END AS activo,
										u.activo AS activ,
										a.nom_area AS nom_area, 
										a.id AS id_areas,
										p.nom_perfil AS nom_perfil,
										p.id AS id_perfil
									FROM usuario AS u
									INNER JOIN areas a ON u.id_area = a.id
									INNER JOIN perfil p ON u.id_perfil = p.id
									WHERE u.activo = 1";
			$this->query = $this->connect->connect->query($this->sql);
			$rows = $this->query->fetchAll(PDO::FETCH_CLASS);
			if($this->query->rowCount() > 0) {	
				$this->res['data'] = $rows;
				$this->res['status'] = 200;
			} else {
				$this->res['status'] = 201;
			}
		} catch (Exception $e) {
			$this->res['status'] = 500;
		}finally {
			return $this->res;
		}
	}

	public function getAllData()
	{

		try {
			$this->res = array();
			$this->sql = "SELECT id, nom_perfil FROM perfil";
			$this->query = $this->connect->connect->query($this->sql);

			if($this->query->rowCount() > 0) {
				$rowsPerfil = $this->query->fetchAll(PDO::FETCH_CLASS);
				$this->sql2 = "SELECT id, nom_area  FROM areas";
				$this->query2 = $this->connect->connect->query($this->sql2);
			}

			if($this->query2->rowCount() > 0){
				$rowsArea = $this->query2->fetchAll(PDO::FETCH_CLASS);
				$this->res['perfil'] = $rowsPerfil;
				$this->res['areas']  = $rowsArea;
				$this->res['status'] = 200;
			}
		} catch (Exception $e) {
			$this->res['status'] = 500;
		} finally {
			return $this->res;
		}

	}
}