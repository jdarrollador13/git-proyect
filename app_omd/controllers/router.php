<?php
require_once '../models/request.php';
require_once '../models/consults.php';
$access = $_REQUEST['accion'];
$resJson;

try {
	switch ($access) {
		case 'validate':
			$addRequest= new Request(json_decode(file_get_contents('php://input'), true));
			$addRequest->getREquest();
			$getConsult = new Consults();
			$resJson = $getConsult->authUsers($addRequest);
			# code...
		break;
		case 'getUsers':
			$getConsult = new Consults();
			$resJson = $getConsult->getUsers();
			# code...
		break;
		case 'getAllData':
			$getConsult = new Consults();
			$resJson = $getConsult->getAllData();
			# code...
		break;
	}
	
} catch (Exception $e) {
	//echo json_encode($e);
} finally {
	//var_dump($resJson);
	echo json_encode($resJson);
}